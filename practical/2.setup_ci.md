# Setup the CI of the forked project

The settings described below are not enabled by default and are not propagated when you fork a project.

## Enable project CI/CD feature

This project requires Gitlab CI/CD.
In order to enable the CI/CD project feature:

- On the left sidebar in the Gitlab interface, go to **Settings** → **General**.
- Expand **Visibility, project features, permissions**.
- In the **Repository** section, turn on **CI/CD**.
- Select **Save changes**.

These steps are given in the
[Gitlab documentation](https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html#enable-cicd-in-a-project).

## Enable shared runners

Shared runners are existing runners provided by ci.inria.fr where jobs can be executed in docker containers.
To activate the shared runners on your project:

- On the left sidebar in the Gitlab interface, go to **Settings** → **CI/CD**.
  If you don't have a **CI/CD** item, then you should
  [enable project CI/CD feature](#enable-project-cicd-feature)
  first.
- Expand **Runners**.
- In the **Shared runners** section,
  turn on **Enable shared runners for this project**.

These steps are given in the
[Gitlab documentation](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#enable-shared-runners-for-a-project).

## Enable Gitlab pages

Gitlab pages helps you publish your project's static html pages. It is enabled by
default for a new project. Its visibility depends on the visibility of the
project. For private projects, the visibility of the pages is limited to *Only
Project Members*. It is possible to change the visibility to *Everyone With Access*.

- On the left sidebar in the Gitlab interface, go to **Settings** → **General**.
- Expand **Visibility, project features, permissions**.
- Turn on **Pages**.
- Adjust visibility.
- Select **Save changes**.

## Enable container registry

The container registry is able to store and use your own docker containers.
Container registry requires to be enabled by doing:

- On the left sidebar in the Gitlab interface, go to **Settings** → **General**.
- Expand **Visibility, project features, permissions**.
- Turn on **Container registry**.
- Select **Save changes**.

Gitlab container registry is documented in the
[Gitlab documentation](https://docs.gitlab.com/ee/user/packages/container_registry/).

***

[go to next](3.yml_for_unitary_tests.md)
